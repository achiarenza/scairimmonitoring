﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ScairimMonitoring.Models;
using ScairimCommons;
using System.Data;
using ScairimMonitoring.Functions;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace ScairimMonitoring.Controllers
{
    [Route("api")]
    [ApiController]
    public class ScairimMonitoringController : ControllerBase
    {
        private static ScairimLogManager logger;
        private static string connString;
        private static EmailConfig emailConf;
        private static ScairimSecurity security;

        public ScairimMonitoringController(IConfiguration configuration)
        {
            if(string.IsNullOrEmpty(connString))
                connString = configuration["ConnectionStrings:Postgresql"];
            if(logger != null)
                logger = new ScairimLogManager(configuration["Logging:LogLevel:Default"], "ScairimMonitoring");
            if (security != null)
                security = new ScairimSecurity(configuration.GetSection("AdmAgent").Get<string[]>(), configuration.GetSection("ReportAgent").Get<string[]>());
            if (emailConf != null)
                emailConf = configuration.GetSection("EmailConfig").Get<EmailConfig>();
        }

        // GET: api/<ScairimMonitoringController>
        [HttpGet]
        public Result Get()
        {
            return GetResult("SUCCESS", "OK", 0, 0);
        }

        // GET api/CustomerReport?customer=ee&email=a.chiarenza@mcube.it
        [HttpGet("CustomerReport")]
        public Result CustomerReport(string customer, string email, bool storeId = false, bool customercolumn = false, bool resolution = false, bool diskspace = false)
        {
            try
            {
                if (!security.ReportAuthorized(Request.Headers["secret"]))
                {
                    logger.Log("Admin request Not authorized", EventLogEntryType.Warning);
                    return GetResult("ERROR", "Not Authorized", customer.Split(",").Count<string>(), 0);
                }

                List<LogEntry> resLogEntry = new List<LogEntry>();
                ContentManager currentCM = new ContentManager();
                Customer _customer = new Customer();
                using ScairimDBManager dbm = new ScairimDBManager(connString);

                _customer = dbm.GetCustomer(customer);
                currentCM = dbm.GetContentManager(_customer.id_cm);
                resLogEntry = dbm.GetLogEntriesFromCM(currentCM, _customer);

                DataTable dtgridExport = ExportLogEntry(resLogEntry, currentCM, storeId, customercolumn, resolution, diskspace);

                ScairimReport report = new ScairimReport(emailConf);
                report.SendReport(email, dtgridExport, currentCM, _customer);
            }
            catch (Exception e)
            {
                logger.Log(e.Message + " " + e.StackTrace, EventLogEntryType.Error);
                return GetResult("ERROR", e.Message + " " + e.StackTrace, customer.Split(",").Count<string>(), 0);
            }

            return GetResult("SUCCESS", "Done", customer.Split(",").Count<string>(), email.Split(",").Count<string>());
        }        

        // GET api/Report?cm=test&email=a.chiarenza@mcube.it&storeid=true
        [HttpGet("Report")]
        public Result Report(string cm, string email, bool storeId = false, bool customercolumn = false, bool resolution = false, bool diskspace = false)
        {           
            try
            {
                if (!security.ReportAuthorized(Request.Headers["secret"]))
                {
                    logger.Log("Admin request Not authorized", EventLogEntryType.Warning);
                    return GetResult("ERROR", "Not Authorized", cm.Split(",").Count<string>(), 0);
                }

                List<LogEntry> resLogEntry = new List<LogEntry>();
                ContentManager currentCM = new ContentManager();
                using ScairimDBManager dbm = new ScairimDBManager(connString);

                currentCM = dbm.GetContentManager(cm);
                resLogEntry = dbm.GetLogEntriesFromCM(currentCM);

                DataTable dtgridExport = ExportLogEntry(resLogEntry, currentCM, storeId, customercolumn, resolution, diskspace);

                ScairimReport report = new ScairimReport(emailConf);
                report.SendReport(email, dtgridExport, currentCM);
            }
            catch (Exception e)
            {
                logger.Log(e.Message + " " + e.StackTrace, EventLogEntryType.Error);
                return GetResult("ERROR", e.Message + " " + e.StackTrace, cm.Split(",").Count<string>(), 0);
            }

            return GetResult("SUCCESS", "Done", cm.Split(",").Count<string>(), email.Split(",").Count<string>());
        }

        private DataTable ExportLogEntry(List<LogEntry> lsLogEntry, ContentManager cm, bool PlayerStoreID = false, 
            bool customerColumn = false, bool resolution = false, bool diskspace = false)
        {
            int minGb;
            bool offlineOnly;
            string SPCtagValue;
            string SPCticket;

            DataTable dtLogEntry = new DataTable();

            if (customerColumn) dtLogEntry.Columns.Add("Customer");
            if (PlayerStoreID) dtLogEntry.Columns.Add("Store ID");
            dtLogEntry.Columns.Add("Name");
            dtLogEntry.Columns.Add("Groups");
            dtLogEntry.Columns.Add("Description");
            dtLogEntry.Columns.Add("Status");
            dtLogEntry.Columns.Add("Last HeartBeat");
            if (resolution)
            {
                dtLogEntry.Columns.Add("Screen Resolution");
                dtLogEntry.Columns.Add("Channel Resolution");
            }
            if (diskspace) dtLogEntry.Columns.Add("Disk Space");
            dtLogEntry.Columns.Add("Clock Offset");
            dtLogEntry.Columns.Add("Ongoing Issue");
            dtLogEntry.Columns.Add("Every Issues");
            dtLogEntry.Columns.Add("Ticket");

            foreach (LogEntry entry in lsLogEntry)
            {
                try
                {
                    SPCtagValue = Commons.Get_HTMLTag_Single("SPC", entry.description);
                    SPCticket = Commons.Get_HTMLTag_Single("TK", entry.description);

                    string[] diskToSkip = { "q" };

                    string[] toSkip = { "uff_", "uff ", "test_", "test " };
                    if (toSkip.Any(entry.player_name.ToLower().Contains) || SPCtagValue == "skip")
                    {
                        continue;
                    }

                    //set check variables to default
                    minGb = 5;
                    offlineOnly = false;

                    //check variables values
                    if (SPCtagValue.Contains("diskAlert") && int.TryParse(SPCtagValue.Split("_")[1], out _))
                    {
                        minGb = int.Parse(SPCtagValue.Split("_")[1]);
                    }
                    if (SPCtagValue.Contains("offlineOnly"))
                    {
                        offlineOnly = true;
                    }

                    DataRow row = dtLogEntry.NewRow();

                    if (customerColumn)
                    {
                        row["Customer"] = entry.customer;
                    }

                    if (PlayerStoreID)
                    {
                        row["Store ID"] = entry.player_name.Split("_")[0];
                    }
                    row["Name"] = entry.player_name;
                    row["Groups"] = Commons.StringListToString(entry.groups);
                    row["Description"] = entry.description;
                    row["Status"] = entry.status;
                    if (entry.lastHeartbeat != null)
                    {
                        row["Last HeartBeat"] = TimeZoneInfo.ConvertTimeFromUtc((DateTime)entry.lastHeartbeat, 
                            TimeZoneInfo.FindSystemTimeZoneById(cm.timezone)).ToString("yyyy/MM/dd HH:mm:ss");
                    }
                    if (resolution)
                    {
                        row["Screen Resolution"] = entry.screenResolution;
                        row["Channel Resolution"] = Commons.StringListToString(entry.channelRes);
                    }
                    if (diskspace)
                    {
                        row["Disk Space"] = ParseDiskSpace(entry.diskSpace);
                    }                        
                    row["Clock Offset"] = GetClockOffset(entry.clockOffset).ToString() + " minutes";

                    //Calcolo dei problemi in corso sul player
                    string ongoingIssue = "";
                    string currentIssue = "";
                    List<string> everyIssue = new List<string>();

                    //Check Offline
                    DateTime now = DateTime.Now.ToUniversalTime();
                                        
                    if (entry.status == "HEARTBEAT_OVERDUE")
                    {
                        if ((now - entry.lastHeartbeat) < new TimeSpan(0, 20, 0, 0))
                        {
                            currentIssue = "Offline < 20 Hours";
                        }
                        else if ((now - entry.lastHeartbeat) < new TimeSpan(0, 44, 0, 0))
                        {
                            currentIssue = "Offline between 20 and 44 Hours";
                        }
                        else if ((now - entry.lastHeartbeat) < new TimeSpan(0, 72, 0, 0))
                        {
                            currentIssue = "Offline between 44 and 72 Hours";
                        }
                        else if ((now - entry.lastHeartbeat) < new TimeSpan(20, 0, 0, 0))
                        {
                            currentIssue = "Offline < 20 Days";
                        }
                        else
                        {
                            currentIssue = "Offline > 20 Days";
                        }
                        ongoingIssue = currentIssue;
                        everyIssue.Add(currentIssue);
                    }
                    if (!offlineOnly)
                    {
                        //Check Inactive
                        if (entry.status == "INACTIVE")
                        {
                            currentIssue = "Inactive";
                            ongoingIssue = currentIssue;
                            everyIssue.Add(currentIssue);
                        }

                        //Check Disk Space
                        if (diskspace) 
                        {
                            //entry.diskSpace = "C: 45572730880, Q: 3538214912"
                            bool foundIssue = false;
                            foreach (string str in Commons.StringToStringList(entry.diskSpace, ","))
                            {
                                if (diskToSkip.Any(str.ToLower().Contains))
                                {
                                    continue;
                                }
                                if (str.Contains(":") && (long.Parse(str.Split(":")[1].Trim()) / 1000000000 < minGb))
                                {
                                    foundIssue = true;
                                }
                            }
                            if (foundIssue)
                            {
                                currentIssue = "Low disk space";
                                everyIssue.Add(currentIssue);
                                if (ongoingIssue == "")
                                {
                                    ongoingIssue = currentIssue;
                                }
                            }
                        }

                        //Check Resolution
                        if (resolution)
                        {
                            string FoundRes = "";
                            string ExpectedRes;
                            if (entry.screenResolution != null && entry.screenResolution != "")
                            {
                                if (entry.screenResolution.ToLower().Contains("monitor"))
                                {
                                    FoundRes = GetMonitorRes(entry.screenResolution);
                                }
                                else if (entry.screenResolution.ToLower().Contains("videocontroller"))
                                {
                                    FoundRes = GetVideoControllerRes(entry.screenResolution);
                                }
                                else
                                {
                                    FoundRes = entry.screenResolution;
                                }
                            }
                            if (entry.description != null && entry.description.Contains("SPC_res"))
                            {
                                ExpectedRes = Commons.Get_HTMLTag_Single("SPC_res", entry.description);
                            }
                            else
                            {
                                ExpectedRes = Commons.StringListToString(entry.channelRes);
                                //#warning hardcoded value
                                if (cm.label == "ee")
                                {
                                    ExpectedRes = ExpectedRes.Replace("1280x720;1920x1080", "1280x800;3840x2160");
                                }
                            }
                            if (FoundRes != ExpectedRes)
                            {
                                if (FoundRes == "")
                                {
                                    currentIssue = "Missing Screenshot Module";
                                }
                                else
                                {
                                    currentIssue = "Wrong Resolution";
                                }
                                if (ExpectedRes == "")
                                {
                                    currentIssue = "Player without channel";
                                }
                                everyIssue.Add(currentIssue);
                                if (ongoingIssue == "")
                                {
                                    ongoingIssue = currentIssue;
                                }
                            }
                        }
                        
                        //Check Clock Offset
                        if (GetClockOffset(entry.clockOffset) > 5)
                        {
                            currentIssue = "Wrong clock offset";
                            everyIssue.Add(currentIssue);
                            if (ongoingIssue == "")
                            {
                                ongoingIssue = currentIssue;
                            }
                        }
                    }
                    //Check Ok with ZZZ
                    if (entry.player_name.ToLower().Contains("zzz"))
                    {
                        if (ongoingIssue == "")
                        {
                            currentIssue = "Player OK with ZZZ";
                            everyIssue.Add(currentIssue);
                            ongoingIssue = currentIssue;
                        }
                        else 
                        {
                            continue;
                        }
                    }
                    //Check Ok with Ticket
                    if (SPCticket != "" && ongoingIssue == "")
                    {
                        currentIssue = "Player OK with Ticket";
                        everyIssue.Add(currentIssue);
                        ongoingIssue = currentIssue;
                    }
                    if (ongoingIssue == "" && SPCticket == "")
                    {
                        continue;
                    }

                    row["Ongoing Issue"] = ongoingIssue;
                    row["Every Issues"] = Commons.StringListToString(everyIssue, ",");
                    row["Ticket"] = SPCticket;

                    dtLogEntry.Rows.Add(row);
                }
                catch (Exception e)
                {
                    logger.Log(e.Message + " " + e.StackTrace, EventLogEntryType.Error);
                    continue;
                }                
            }

            return dtLogEntry;
        }

        private Result GetResult(string result, string description, int requests, int managed)
        {
            Result res = new Result
            {
                result = result,
                data = new ReturnedData { description = description, requests = requests, processed = managed }
            };
            return res;
        }

        private string GetMonitorRes(string resolution)
        {
            //Monitor1: 3840x2160 - Monitor2 (MAIN): 1280x800
            string res = "";
            foreach (string str in resolution.Split("-"))
            {
                if (str.ToLower().Contains("main"))
                {
                    res = str.Split(":")[1].Trim();
                }
            }
            foreach (string str in resolution.Split("-"))
            {
                if (str.ToLower().Contains("main"))
                {
                    continue;
                }
                if (res == "")
                {
                    res = str.Split(":")[1].Trim();
                }
                else
                {
                    res = res + ";" + str.Split(":")[1].Trim();
                }
            }
            return res;
        }

        private string GetVideoControllerRes(string resolution, string separator = ";")
        {
        //VideoController1: 1920x1080VideoController2: 1024x768
            string res = Regex.Replace(resolution, "VideoController\\d: ", separator)[1..].Replace("\n", "");

            return res;
        }        

        private string ParseDiskSpace(string diskspace)
        {
            //entry.diskSpace = "C: 45572730880, Q: 3538214912"
            string res = "";

            if (diskspace == "")
            {
                return res;
            }

            foreach (string str in Commons.StringToStringList(diskspace, ","))
            {
                if (res == "")
                {
                    res = str.Split(" ")[0] + (long.Parse(str.Split(" ")[1]) / 1000000000) + "GB";
                }
                else
                {
                    res = res + ", " + str.Split(" ")[0] + (long.Parse(str.Split(" ")[1]) / 1000000000) + "GB";
                }
            }

            return res;
        }

        private long GetClockOffset(long? offset)
        {
            if (offset == null)
            {
                return 0;
            }
            return (long)(Math.Abs((double)offset) / 60);
        }
    }
}
