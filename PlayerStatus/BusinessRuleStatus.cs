﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScairimMonitoring.PlayerStatus
{
    public class BusinessRuleStatus
    {
        public string Status { get; set; }
        public bool IsIssue { get; set; }        
        public string IssueDescription { get; set; }
        public string ColumnName { get; set; }
        public int Priority { get; set; }
        public int ColumnPosition { get; set; }

        public BusinessRuleStatus() { }

        public BusinessRuleStatus(string Status)
        {
            this.Status = Status;
        }

        public BusinessRuleStatus(string Status, bool IsIssue, string IssueDescription, string ColumnName, int Priority, int ColumnPosition) 
        {
            this.Status = Status;
            this.IsIssue = IsIssue;            
            this.IssueDescription = IssueDescription;
            this.ColumnName = ColumnName;
            this.Priority = Priority;
            this.ColumnPosition = ColumnPosition;            
        }
    }
}
