﻿using ScairimMonitoring.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScairimMonitoring.PlayerStatus
{
    public abstract class BusinessRule
    {
        protected BusinessRulesParameters Parameters;
        protected string currentIssue;
        protected bool IsIssue;

        public BusinessRule(BusinessRulesParameters Parameters)
        {
            this.Parameters = Parameters;
            currentIssue = "";
            IsIssue = false;
        }

        public abstract BusinessRuleStatus Run(LogEntry logEntry, ContentManager cm);
    }
}
