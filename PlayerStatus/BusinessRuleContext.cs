﻿using ScairimMonitoring.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScairimMonitoring.PlayerStatus
{
    public class BusinessRuleContext
    {
        public ContentManager cm { get; set; }
        public BusinessRulesParameters BusinessRulesParameters { get; set; }
    }
}
