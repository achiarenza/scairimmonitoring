﻿using ScairimMonitoring.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScairimMonitoring.PlayerStatus
{
    public class ActivityBusinessRule : BusinessRule
    {
        private readonly new ActivityParameters Parameters;
        private readonly string Status;

        public ActivityBusinessRule(ActivityParameters Parameters, string Status) : base(Parameters)
        {
            this.Parameters = Parameters;
            this.Status = Status;
        }

        public override BusinessRuleStatus Run(LogEntry logEntry, ContentManager cm)
        {
            if (Status == "INACTIVE")
            {
                currentIssue = "Inactive";
                IsIssue = true;
            }
            return new BusinessRuleStatus(Status, IsIssue, currentIssue, "Status", Parameters.Priority, Parameters.ColumnPosition);
        }
    }
}
