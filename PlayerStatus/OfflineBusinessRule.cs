﻿using ScairimMonitoring.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScairimMonitoring.PlayerStatus
{
    public class OfflineBusinessRule : BusinessRule
    {
        private readonly new OfflineParameters Parameters;
        private const string DATE_PATTERN = "yyyy/MM/dd HH:mm:ss";

        public OfflineBusinessRule(OfflineParameters Parameters) : base(Parameters)
        {
            this.Parameters = Parameters;
        }

        public override BusinessRuleStatus Run(LogEntry logEntry, ContentManager cm)
        {
            DateTime now = DateTime.Now.ToUniversalTime();
            string resLastHeartBeat = TimeZoneInfo.ConvertTimeFromUtc((DateTime)logEntry.lastHeartbeat,
                            TimeZoneInfo.FindSystemTimeZoneById(cm.timezone)).ToString(DATE_PATTERN);

            if ((now - logEntry.lastHeartbeat) < new TimeSpan(0, Parameters.OfflineHours, 0, 0))
            {
                currentIssue = "Offline < " + Parameters.OfflineHours + " Hours";
            }
            else
            {
                foreach (BetweenHours bh in Parameters.BetweenHours)
                {
                    if ((now - logEntry.lastHeartbeat) > new TimeSpan(0, bh.MinHour, 0, 0) && (now - logEntry.lastHeartbeat) < new TimeSpan(0, bh.MaxHour, 0, 0))
                    {
                        currentIssue = "Offline between " + bh.MinHour + " and " + bh.MaxHour + " Hours";
                    }
                }                
            }            
            if (currentIssue == "" && (now - logEntry.lastHeartbeat) < new TimeSpan(Parameters.OfflineDays, 0, 0, 0))
            {
                currentIssue = "Offline < " + Parameters.OfflineDays + " Days";
            }
            else
            {
                currentIssue = "Offline > " + Parameters.OfflineDays + " Days";
            }

            if (currentIssue != "") IsIssue = true;

            return new BusinessRuleStatus(resLastHeartBeat, IsIssue, currentIssue, "Last HeartBeat", Parameters.Priority, Parameters.ColumnPosition);
        }
    }
}
