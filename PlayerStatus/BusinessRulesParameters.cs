﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScairimMonitoring.PlayerStatus
{
    public class BusinessRulesParameters
    {
        public int Priority { get; set; }
        public int ColumnPosition { get; set; }
    }
}
