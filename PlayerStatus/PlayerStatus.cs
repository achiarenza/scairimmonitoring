﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScairimMonitoring.PlayerStatus
{
    public class PlayerStatus
    {
        public List<BusinessRuleStatus> BrsList { get; set; }

        public PlayerStatus()
        {
            BrsList = new List<BusinessRuleStatus>();
        }
    }
}
