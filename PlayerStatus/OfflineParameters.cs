﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScairimMonitoring.PlayerStatus
{
    public class OfflineParameters : BusinessRulesParameters
    {
        public int OfflineHours { get; set; }
        public List<BetweenHours> BetweenHours { get; set; } 
        public int OfflineDays { get; set; }
    }

    public class BetweenHours
    {
        public int MinHour { get; set; }
        public int MaxHour { get; set; }
    }
}
