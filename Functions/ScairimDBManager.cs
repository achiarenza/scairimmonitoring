﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ScairimMonitoring.Models;
using ScairimCommons;

namespace ScairimMonitoring.Functions
{
    public class ScairimDBManager : IDisposable
    {
        private readonly NpgsqlConnection conn;

        public ScairimDBManager(string connString)
        {
            conn = new NpgsqlConnection(connString);
            conn.Open();
        }

        public void Dispose()
        {
            conn.Close();
            conn.Dispose();
        }

        public Customer GetCustomer(string customer)
        {            
            const string sql = "SELECT * FROM customers WHERE name = @name";

            using (var cmd = new NpgsqlCommand(sql, conn))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@name", NpgsqlTypes.NpgsqlDbType.Text));
                cmd.Parameters["@name"].Value = DBvalue(customer);

                using (var reader = cmd.ExecuteReader())
                {
                    reader.Read();
                    return CustomerReader(reader);
                }
            }
        }

        private Customer CustomerReader(NpgsqlDataReader reader)
        {
            Customer c = new Customer
            {
                id = reader.GetInt32(reader.GetOrdinal("id")),
                id_cm = reader.GetInt32(reader.GetOrdinal("id_cm")),
                name = reader.GetString(reader.GetOrdinal("name")),
                brand = reader.GetString(reader.GetOrdinal("brand"))
            };

            return c;
        }

        public ContentManager GetContentManager(string cmLabel)
        {
            const string sql = "SELECT * FROM cms WHERE label = @label";

            using (var cmd = new NpgsqlCommand(sql, conn))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@label", NpgsqlTypes.NpgsqlDbType.Text));
                cmd.Parameters["@label"].Value = DBvalue(cmLabel);

                using (var reader = cmd.ExecuteReader())
                {
                    reader.Read();
                    return ContentManagerReader(reader);
                }
            }
        }

        public ContentManager GetContentManager(int id_cm)
        {
            const string sql = "SELECT * FROM cms WHERE id = @id_cm";

            using (var cmd = new NpgsqlCommand(sql, conn))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@id_cm", NpgsqlTypes.NpgsqlDbType.Integer));
                cmd.Parameters["@id_cm"].Value = DBvalue(id_cm);

                using (var reader = cmd.ExecuteReader())
                {
                    reader.Read();
                    return ContentManagerReader(reader);
                }
            }
        }

        private ContentManager ContentManagerReader(NpgsqlDataReader reader)
        {
            ContentManager cm = new ContentManager();

            cm.id = reader.GetInt32(reader.GetOrdinal("id"));
            cm.address = reader.GetString(reader.GetOrdinal("address"));
            cm.label = reader.GetString(reader.GetOrdinal("label"));
            cm.platform = reader.GetString(reader.GetOrdinal("platform"));
            cm.timezone = reader.GetString(reader.GetOrdinal("timezone"));

            return cm;
        }

        public List<LogEntry> GetLogEntriesFromCM(ContentManager cm)
        {
            List<LogEntry> res = new List<LogEntry>();
            const string sql = "SELECT * FROM logentry WHERE id_cm = @id_cm";

            using (var cmd = new NpgsqlCommand(sql, conn))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@id_cm", NpgsqlTypes.NpgsqlDbType.Bigint));
                cmd.Parameters["@id_cm"].Value = DBvalue(cm.id);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        res.Add(LogEntryReader(reader));
                    }
                }
            }

            AddHardwaresToLogEntries(res);

            return res;
        }

        public List<LogEntry> GetLogEntriesFromCM(ContentManager cm, Customer c)
        {
            List<LogEntry> res = new List<LogEntry>();
            const string sql = "SELECT * FROM logentry WHERE id_cm = @id_cm AND id_customer = @id_customer";

            using (var cmd = new NpgsqlCommand(sql, conn))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@id_cm", NpgsqlTypes.NpgsqlDbType.Bigint));
                cmd.Parameters.Add(new NpgsqlParameter("@id_customer", NpgsqlTypes.NpgsqlDbType.Bigint));
                cmd.Parameters["@id_cm"].Value = DBvalue(cm.id);
                cmd.Parameters["@id_customer"].Value = DBvalue(c.id);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        res.Add(LogEntryReader(reader));
                    }
                }
            }

            AddHardwaresToLogEntries(res);

            return res;
        }

        private LogEntry LogEntryReader(NpgsqlDataReader reader)
        {
            LogEntry l = new LogEntry
            {
                id = reader.GetInt64(reader.GetOrdinal("id")),
                player_name = reader.GetString(reader.GetOrdinal("player_name")),
                uuid = reader.GetString(reader.GetOrdinal("uuid")),
                jsonText = reader.GetString(reader.GetOrdinal("json_text")),
                id_agent = reader.GetInt64(reader.GetOrdinal("id_agent")),
                id_cm = reader.GetInt64(reader.GetOrdinal("id_cm")),
                hardware = new List<Hardware>()
            };
            if (!reader.IsDBNull(reader.GetOrdinal("status"))) l.status = reader.GetString(reader.GetOrdinal("status"));
            if (!reader.IsDBNull(reader.GetOrdinal("description"))) l.description = reader.GetString(reader.GetOrdinal("description"));
            if (!reader.IsDBNull(reader.GetOrdinal("last_heartbeat"))) l.lastHeartbeat = reader.GetDateTime(reader.GetOrdinal("last_heartbeat"));
            if (!reader.IsDBNull(reader.GetOrdinal("timestamp"))) l.timestamp = reader.GetDateTime(reader.GetOrdinal("timestamp"));
            if (!reader.IsDBNull(reader.GetOrdinal("hostname"))) l.hostname = reader.GetString(reader.GetOrdinal("hostname"));
            if (!reader.IsDBNull(reader.GetOrdinal("timezone"))) l.timezone = reader.GetString(reader.GetOrdinal("timezone"));
            if (!reader.IsDBNull(reader.GetOrdinal("clock_offset"))) l.clockOffset = reader.GetInt64(reader.GetOrdinal("clock_offset"));
            if (!reader.IsDBNull(reader.GetOrdinal("id_customer"))) l.id_customer = reader.GetInt64(reader.GetOrdinal("id_customer"));
            if (!reader.IsDBNull(reader.GetOrdinal("last_booted"))) l.lastBooted = reader.GetDateTime(reader.GetOrdinal("last_booted"));
            if (!reader.IsDBNull(reader.GetOrdinal("current_datetime"))) l.currentDatetime = reader.GetDateTime(reader.GetOrdinal("current_datetime"));
            if (!reader.IsDBNull(reader.GetOrdinal("disk_space"))) l.diskSpace = reader.GetString(reader.GetOrdinal("disk_space"));
            if (!reader.IsDBNull(reader.GetOrdinal("inventory_status"))) l.inventoryStatus = reader.GetString(reader.GetOrdinal("inventory_status"));
            if (!reader.IsDBNull(reader.GetOrdinal("screen_resolution"))) l.screenResolution = reader.GetString(reader.GetOrdinal("screen_resolution"));
            if (!reader.IsDBNull(reader.GetOrdinal("customer"))) l.customer = reader.GetString(reader.GetOrdinal("customer"));
            if (!reader.IsDBNull(reader.GetOrdinal("remaining_items"))) l.remainingItems = reader.GetInt32(reader.GetOrdinal("remaining_items"));
            if (!reader.IsDBNull(reader.GetOrdinal("remaining_mb"))) l.remainingMB = reader.GetDouble(reader.GetOrdinal("remaining_mb"));
            if (!reader.IsDBNull(reader.GetOrdinal("frameset_res"))) l.framesetRes = Commons.StringToStringList(reader.GetString(reader.GetOrdinal("frameset_res")));
            if (!reader.IsDBNull(reader.GetOrdinal("channel_res"))) l.channelRes = Commons.StringToStringList(reader.GetString(reader.GetOrdinal("channel_res")));
            if (!reader.IsDBNull(reader.GetOrdinal("channel"))) l.channel = Commons.StringToStringList(reader.GetString(reader.GetOrdinal("channel")));
            if (!reader.IsDBNull(reader.GetOrdinal("last_update"))) l.lastUpdate = reader.GetDateTime(reader.GetOrdinal("last_update"));
            if (!reader.IsDBNull(reader.GetOrdinal("player_timestamp"))) l.playerTimestamp = reader.GetDateTime(reader.GetOrdinal("player_timestamp"));
            if (!reader.IsDBNull(reader.GetOrdinal("groups"))) l.groups = Commons.StringToStringList(reader.GetString(reader.GetOrdinal("groups")));

            return l;
        }

        private void AddHardwaresToLogEntries(List<LogEntry> logEntries)
        {
            long[] id_entries = logEntries.Select(x => x.id).ToArray();
            List<Hardware> hardwares = GetHardwares(id_entries);
            foreach (Hardware h in hardwares)
            {
                logEntries.FirstOrDefault(x => x.id == h.id_entry).hardware.Add(h);
            }
        }

        private List<Hardware> GetHardwares(long[] id_entries)
        {
            List<Hardware> res = new List<Hardware>();
            const string sql = "SELECT * FROM hardware WHERE id_entry = ANY(@id_entries)";

            using (var cmd = new NpgsqlCommand(sql, conn))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@id_entries", NpgsqlTypes.NpgsqlDbType.Array | NpgsqlTypes.NpgsqlDbType.Bigint));
                cmd.Parameters["@id_entries"].Value = id_entries;

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Hardware h = HardwareReader(reader);
                        res.Add(h);
                    }
                }
            }

            return res;
        }

        private Hardware HardwareReader(NpgsqlDataReader reader)
        {
            Hardware h = new Hardware
            {
                id = reader.GetInt64(reader.GetOrdinal("id")),
                hwType = reader.GetString(reader.GetOrdinal("hw_type")),
                status = reader.GetString(reader.GetOrdinal("status")),
                serialNumber = reader.GetString(reader.GetOrdinal("serialnumber")),
                jsonText = reader.GetString(reader.GetOrdinal("json_text")),
                timestamp = reader.GetDateTime(reader.GetOrdinal("timestamp")),
                id_entry = reader.GetInt64(reader.GetOrdinal("id_entry"))
            };
            if (!reader.IsDBNull(reader.GetOrdinal("last_heartbeat"))) h.lastHeartbeat = reader.GetDateTime(reader.GetOrdinal("last_heartbeat"));

            return h;
        }

        private object DBvalue(object value)
        {
            if (value == null)
            {
                return DBNull.Value;
            }
            else
            {
                return value;
            }
        }        
    }
}
