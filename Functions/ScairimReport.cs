﻿using ScairimCommons;
using ScairimMonitoring.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace ScairimMonitoring.Functions
{
    public class ScairimReport
    {
        private readonly EmailConfig config;
        private readonly string excelPath;

        public ScairimReport(EmailConfig config)
        {
            this.config = config;
            excelPath = AppDomain.CurrentDomain.BaseDirectory + "Excel\\";
        }

        public ScairimReport(EmailConfig config, string excelPath)
        {
            this.config = config;
            this.excelPath = excelPath;
        }

        public void SendReport(string receiver, DataTable LogEntries, ContentManager cm, Customer customer)
        {
            string excelFile = GetExcelFileName(excelPath, customer.name);

            ADODB.DataTableToExcelOLEDB(excelFile, LogEntries);

            string subject = "[Proactive Control] " + cm.label.ToUpper() + " - " + customer.name.ToUpper();
            Mail.SendEmail(config.Username, config.Password, config.Sender, receiver, config.SMTP, config.Port, subject, "", excelFile);
        }

        public void SendReport(string receiver, DataTable LogEntries, ContentManager cm)
        {
            string excelFile = GetExcelFileName(excelPath, cm.label);

            ADODB.DataTableToExcelOLEDB(excelFile, LogEntries);

            string subject = "[Proactive Control] " + cm.label.ToUpper();
            Mail.SendEmail(config.Username, config.Password, config.Sender, receiver, config.SMTP, config.Port, subject, "", excelFile);
        }

        private string GetExcelFileName(string path, string net)
        {
            int index = 1;
            while (true)
            {
                if (System.IO.File.Exists(path + "ScairimMonitoring_Report_" + DateTime.Today.ToShortDateString().Replace("/", "-") + "_" + net + "_" + index + ".xlsx"))
                {
                    index += 1;
                }
                else
                {
                    break;
                }
            }
            return path + "ScairimMonitoring_Report_" + DateTime.Today.ToShortDateString().Replace("/", "-") + "_" + net + "_" + index + ".xlsx";
        }
    }
}
