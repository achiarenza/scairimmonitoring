﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScairimMonitoring.Models
{
    public class Result
    {
        public string result { get; set; }
        public ReturnedData data { get; set; }
    }
}
