﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScairimMonitoring.Models
{
    public class EmailConfig
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string SMTP { get; set; }
        public int Port { get; set; }
        public string Sender { get; set; }
    }
}
